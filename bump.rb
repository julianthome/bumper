#!/usr/bin/env ruby
# frozen_string_literal: true

require 'logger'
require 'optparse'
require_relative 'lib/semver_manager'

options = {}
log = Logger.new($stdout)

option_parser = OptionParser.new do |opts|
  opts.banner = "Usage: #{$PROGRAM_NAME} --git-path GITPATH --email EMAIL --name NAME [options]"
  opts.on('-v', '--version-prefix VPFX', 'The version prefix to be used (per default we do not use any)') do |o|
    options[:version_prefix] = o
  end
  opts.on('-g', '--git-path GITPATH', String, 'The git repository location on the local filesystem') do |o|
    options[:git_path] = o
  end
  opts.on('-c', '--clog_file CLOG', String, 'The path to your changelog file') do |o|
    options[:clog_file] = o
  end
  opts.on('-e', '--email EMAIL', String, 'Email address of the commit user') do |o|
    options[:email] = o
  end
  opts.on('-n', '--name NAME', String, 'Name of the commit user') do |o|
    options[:name] = o
  end
  opts.on('-h', '--help', 'Prints this help') do
    puts opts
    exit
  end
end
option_parser.parse!

missing_opts = %i[git_path email name].filter { |o| options[o].nil? || options[o].empty? }.any?
if missing_opts
  puts option_parser.help
  exit 1
end

unless options[:email].match?("^#{URI::MailTo::EMAIL_REGEXP}$")
  puts 'use proper Email for commit user'
  exit 1
end

unless options[:name].match?('^[a-zA-Z0-9]+$')
  puts 'use proper Email for commit user (alphanumeric withoug spaces)'
  exit 1
end

if options[:version_prefix].nil?
  options[:version_prefix] = ''
else
  unless options[:version_prefix].match('[a-zA-Z0-9]{1,10}')
    puts option_parser.help
    exit(-1)
  end
end

unless File.exist?(options[:git_path]) && File.directory?(options[:git_path])
  log.error("make sure #{options[:git_path]} exists and is a directory")
  exit(-1)
end

unless options[:clog_file].nil?
  clog_full_path = File.join(options[:git_path], options[:clog_file])
  unless File.exist?(clog_full_path)
    log.error("invalid changelog file provided: #{clog_full_path}")
    exit(-1)
  end
  options[:clog_full_path] = clog_full_path
end

SemverManager.new(options).autobump

exit(0)
