# Copyright (c) 2019, Julian Thome
#
# Released under the MIT license
# https://opensource.org/licenses/MIT

FROM ruby:3-alpine
MAINTAINER julian.thome.de@gmail.com
RUN apk add bash libffi-dev build-base libxml2-dev git openssh-client
RUN mkdir /opt/bumper
COPY . /opt/bumper
RUN gem install bundler
RUN bundle config set --local frozen 'true'
RUN cd /opt/bumper && bundle install
RUN mkdir /opt/wdir
WORKDIR /opt/wdir
VOLUME ["/opt/wdir"]
CMD ["/bin/bash"]
ENV PATH /opt/bumper:$PATH
