# frozen_string_literal: true

require 'erb'
require 'git'
require 'logger'
require_relative 'cmd'
require_relative 'changelog'

# Git command codes
module GitCmdCode
  BUMP_MAJOR = 'bump_major'
  BUMP_MINOR = 'bump_minor'
  BUMP_PATCH = 'bump_patch'
  SKIP = 'skip ci'

  def self.rexp
    GitCmd.wrap("(#{[BUMP_MAJOR, BUMP_MINOR, BUMP_PATCH, SKIP].join('|')})")
  end

  def self.from_string(string)
    case string.downcase
    when BUMP_MAJOR
      BUMP_MAJOR
    when BUMP_MINOR
      BUMP_MINOR
    when SKIP
      SKIP
    else
      BUMP_PATCH
    end
  end
end

# Git interface class
class GitHandler
  def initialize(options)
    @log = Logger.new($stdout)
    @repo = Git.open(options[:git_path])
    @version_prefix = options[:version_prefix].nil? ? '' : options[:version_prefix]
    @latest_tag = 'version_latest'
    @email = options[:email]
    @name = options[:name]
    @repo.config('user.name', @name)
    @repo.config('user.email', @email)
  end

  def extract_last_version_from_tag
    latest_sha = get_sha_from_tag(@latest_tag)
    return '' if latest_sha.empty?

    # head_sha = @repo.log.first.sha
    # puts head_sha

    semver = @repo.tags.map(&:name)
                  .reject { |tagname| tagname == @latest_tag }
                  .filter { |tagname| tagname.match?("^#{@version_prefix}\\d+\\.\\d+\\.\\d+$") }
                  .map { |tagname| tagname.delete_prefix(@version_prefix) }
                  .sort_by { |v| Gem::Version.new(v) }

    return '' if semver.empty?

    tag_sha = get_sha_from_tag("#{@version_prefix}#{semver.last}")

    if tag_sha != latest_sha
      @log.error("discrepancy between the SHA of #{@latest_tag} (#{latest_sha}) and #{semver.last} (#{tag_sha})")
      return ''
    end

    semver.last
  end

  def extract_command_from_log_message
    matches = @repo.log(1).first.message.match(GitCmdCode.rexp)
    cmd = !matches.nil? ? matches[1] : ''
    GitCmdCode.from_string(cmd)
  end

  def attach_new_version_tag_to_head(new_version, test_only: false)
    @log.info("adding #{new_version} to HEAD")
    begin
      new_version = "#{@version_prefix}#{new_version}"
      @repo.delete_tag(@latest_tag)
      @repo.add_tag(@latest_tag, 'HEAD')
      @repo.add_tag(new_version, 'HEAD')
      @repo.push('origin', "refs/tags/#{new_version}", f: true)
      @repo.push('origin', "refs/tags/#{@latest_tag}", f: true)
    rescue StandardError => e
      @log.error("could not push the tags to origin #{e}")
      exit(-1) unless test_only
    end
  end

  def commit_and_push(file, test_only: false)
    @log.info("adding #{file}")
    begin
      @repo.add(file)
      @repo.commit("changelog updated\n[#{GitCmdCode::SKIP}]")
      @repo.push('origin', 'master') unless test_only
    rescue StandardError => e
      @log.error("could not push the changelog changes to origin #{e}")
      exit(-1) unless test_only
    end
  end

  def filter_clog_msg(clog, text)
    text.split("\n").each do |row|
      next unless row.match?("#{ClogCmdCode.rexp}{[\\(\\)\\w\\.: ]+}")

      # this call cannot fail anymore at this point
      m = row.match(/#{ClogCmdCode.rexp}{([()\w.: ]+)}/)
      clog.add_entry(m[1], m[2])
    end
  end

  def changelog_entry(oldsemver, newsemver)
    cdate = @repo.log.any? ? @repo.log.first.date : Time.now
    clog = ChangeLog.new(newsemver, cdate.strftime('%Y-%m-%d'))

    sha = get_sha_from_tag(oldsemver)

    if sha.empty?
      @log.error("Could not find sha for #{oldsemver}")
      return ''
    end

    @repo.log.each do |logentry|
      break if logentry.to_s == sha

      msg = logentry.message
      filter_clog_msg(clog, msg)
    end

    clog.render_to_md
  end

  private

  def get_sha_from_tag(tag_name)
    return '' if @repo.tags.empty?

    tags = @repo.tags.filter { |tag| tag.name == tag_name }.map(&:to_s)
    return '' if tags.empty?

    tags.first
  end
end
