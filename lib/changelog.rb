# frozen_string_literal: true

require 'erb'
require_relative 'cmd'

# Commands related to changelog management
class ClogCmdCode
  CLOG_ADDED = 'added'
  CLOG_CHANGED = 'changed'
  CLOG_DEPRECATED = 'deprecated'
  CLOG_REMOVED = 'removed'
  CLOG_FIXED = 'fixed'
  CLOG_SECURITY = 'security'

  def self.rexp
    GitCmd.wrap("(#{[CLOG_ADDED, CLOG_CHANGED, CLOG_DEPRECATED, CLOG_REMOVED, CLOG_FIXED, CLOG_SECURITY].join('|')})")
  end
end

# The ChangeLog class represents a single changelog entry
class ChangeLog
  def initialize(semver, date)
    @entries = {
      'semver' => semver,
      'date' => date
    }
    @template = <<~ERB
      #### [<%= clog['semver'] %>] <%= clog['date'] %>
      <% clog.each do |k,v| -%>
      <% if v.instance_of?(Array) -%>
      ##### <%= k.capitalize %>
      <% v.each do |entry| -%>
      - <%= entry %>
      <% end -%>
      <% end -%>
      <% end -%>
    ERB
  end

  def add_entry(code, msg)
    @entries[code] = [] unless @entries.key?(code)
    @entries[code] << msg
  end

  def render_to_md
    ERB.new(@template, trim_mode: '-').result_with_hash(clog: @entries.sort.to_h).to_s
  end
end
